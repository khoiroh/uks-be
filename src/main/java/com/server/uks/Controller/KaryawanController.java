package com.server.uks.Controller;

import com.server.uks.modal.Karyawan;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/karyawan")
public class KaryawanController {

    @Autowired
    KaryawanService karyawanService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<Karyawan> getKaryawanById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(karyawanService.getKaryawan(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<Karyawan> addKaryawan(@RequestBody Karyawan karyawan) {
        return ResponseHelper.ok(karyawanService.addKaryawan(karyawan));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<Karyawan> editKaryawanById(@PathVariable("id") Long id, @RequestBody Karyawan karyawan) {
        return ResponseHelper.ok(karyawanService.editKaryawan(id, karyawan));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteKaryawanById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(karyawanService.deleteKaryawanById(id));}

    @GetMapping("/all-guru")
    public CommonResponse<List<Karyawan>> allKaryawan() {
        return ResponseHelper.ok(karyawanService.allKaryawan());
    }
}
