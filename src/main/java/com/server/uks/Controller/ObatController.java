package com.server.uks.Controller;

import com.server.uks.modal.Obat;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/obat")
public class ObatController {

    @Autowired
    ObatService obatService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<Obat> getObatById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(obatService.getObat(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<Obat> addObat(@RequestBody Obat obat) {
        return ResponseHelper.ok(obatService.addObat(obat));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<Obat> editObatById(@PathVariable("id") Long id, @RequestBody Obat obat) {
        return ResponseHelper.ok(obatService.editObat(id, obat));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteObatById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(obatService.deleteObatById(id));}

    @GetMapping
    public CommonResponse<List<Obat>> allObat() {
        return ResponseHelper.ok(obatService.allObat());
    }

}
