package com.server.uks.Controller;

import com.server.uks.modal.PenangananPertama;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.PenangananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/penanganan")
public class PenangananController {

    @Autowired
    PenangananService penangananService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<PenangananPertama> getPenangananPertamaById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(penangananService.getPenangananPertama(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<PenangananPertama> addPenangananPertama(@RequestBody PenangananPertama penangananPertama) {
        return ResponseHelper.ok(penangananService.addPenangananPertama(penangananPertama));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<PenangananPertama> editPenangananPertamaById(@PathVariable("id") Long id, @RequestBody PenangananPertama penangananPertama) {
        return ResponseHelper.ok(penangananService.editPenangananPertama(id, penangananPertama));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deletePenangananPertamaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(penangananService.deletePenangananPertamaById(id));}

    @GetMapping
    public CommonResponse<List<PenangananPertama>> allPenangananPertama() {
        return ResponseHelper.ok(penangananService.allPenangananPertama());
    }

}
