package com.server.uks.Controller;

import com.server.uks.modal.Login;
import com.server.uks.modal.Register;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.RegisterService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/uks")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/register")
    public CommonResponse<Register> registrasi(@RequestBody Register sekolah) {
        return ResponseHelper.ok(registerService.registrasi(sekolah));
    }

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.ok(registerService.login(login));
    }


    @GetMapping("/{id}")
    public CommonResponse<Register> getById(@PathVariable Long id) {
        return ResponseHelper.ok(registerService.getById(id));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteSekolah(@PathVariable("id") Long id) {
        return ResponseHelper.ok(registerService.deleteSekolah(id));
    }
    @GetMapping
    public CommonResponse<Page<Register>> getAll(@RequestParam(name = "query", required = false) String query, Long page) {
        return ResponseHelper.ok(registerService.getAll(query == null ? "" : query, page));
    }
}
