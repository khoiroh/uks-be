package com.server.uks.Controller;

import com.server.uks.modal.Tindakan;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tindakan")
public class TindakanController {

    @Autowired
    TindakanService tindakanService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<Tindakan> getTindakanById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(tindakanService.getTindakan(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<Tindakan> addTindakan(@RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.addTindakan(tindakan));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<Tindakan> editTindakanById(@PathVariable("id") Long id, @RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.editTindakan(id, tindakan));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteTindakanById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.deleteTindakanById(id));}

    @GetMapping
    public CommonResponse<List<Tindakan>> allTindakan() {
        return ResponseHelper.ok(tindakanService.allTindakan());
    }
}
