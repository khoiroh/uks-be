package com.server.uks.Controller;

import com.server.uks.modal.Siswa;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/siswa")
public class SiswaController {

    @Autowired
    SiswaService siswaService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<Siswa> getGuruById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(siswaService.getSiswa(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<Siswa> addGuru(@RequestBody Siswa siswa) {
        return ResponseHelper.ok(siswaService.addSiswa(siswa));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<Siswa> editGuruById(@PathVariable("id") Long id, @RequestBody Siswa siswa) {
        return ResponseHelper.ok(siswaService.editSiswa(id, siswa));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteGuruById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(siswaService.deleteSiswaById(id));}

    @GetMapping
    public CommonResponse<List<Siswa>> allGuru() {
        return ResponseHelper.ok(siswaService.allGuru());
    }
}
