package com.server.uks.Controller;

import com.server.uks.modal.Diagnosa;
import com.server.uks.response.CommonResponse;
import com.server.uks.response.ResponseHelper;
import com.server.uks.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/diagnosa")
public class DiagnosaController {

    @Autowired
    DiagnosaService diagnosaService;

    @GetMapping("/{id}") //untuk melihat sesaui id
    public CommonResponse<Diagnosa> getDiagnosaById(@PathVariable("id")Long id) {
        return ResponseHelper.ok(diagnosaService.getDiagnosa(id)) ;
    }

    @PostMapping // untuk mengepost data
    public CommonResponse<Diagnosa> addDiagnosa(@RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.addDiagnosa(diagnosa));
    }

    @PutMapping("/{id}") // untuk mengedit data sesuai id
    public CommonResponse<Diagnosa> editDiagnosaById(@PathVariable("id") Long id, @RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.editDiagnosa(id, diagnosa));
    }

    @DeleteMapping("/{id}") // untuk menghapus data sesuai id
    public CommonResponse <?> deleteDiagnosaById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.deleteDiagnosaById(id));}

    @GetMapping
    public CommonResponse<List<Diagnosa>> allDiagnosa() {
        return ResponseHelper.ok(diagnosaService.allDiagnosa());
    }
}
