package com.server.uks.service;


import com.server.uks.modal.PenangananPertama;

import java.util.List;
import java.util.Map;

public interface PenangananService {

    PenangananPertama getPenangananPertama(Long id);

    PenangananPertama addPenangananPertama(PenangananPertama penangananPertama);

    PenangananPertama editPenangananPertama(Long id,PenangananPertama penangananPertama);

    Map<String ,Boolean> deletePenangananPertamaById(Long id);

    List<PenangananPertama> allPenangananPertama();
}
