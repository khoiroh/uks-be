package com.server.uks.service;

import com.server.uks.modal.Karyawan;

import java.util.List;
import java.util.Map;

public interface KaryawanService {

    Karyawan getKaryawan(Long id);

    Karyawan addKaryawan(Karyawan karyawan);

    Karyawan editKaryawan(Long id,Karyawan karyawan);

    Map<String ,Boolean> deleteKaryawanById(Long id);

    List<Karyawan> allKaryawan();
}
