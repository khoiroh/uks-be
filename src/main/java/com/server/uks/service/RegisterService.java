package com.server.uks.service;

import com.server.uks.modal.Login;
import com.server.uks.modal.Register;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface RegisterService {

    Register registrasi(Register sekolah);

    Map<String, Object> login(Login login);

    Register getById(Long id);

    Register update(Long id, Register sekolah);

    Map<String, Boolean> deleteSekolah(Long id);

    Page<Register> getAll(String query, Long page);

}
