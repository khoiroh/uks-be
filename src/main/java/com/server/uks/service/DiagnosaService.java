package com.server.uks.service;

import com.server.uks.modal.Diagnosa;

import java.util.List;
import java.util.Map;

public interface DiagnosaService {

    Diagnosa getDiagnosa(Long id);

    Diagnosa addDiagnosa(Diagnosa diagnosa);

    Diagnosa editDiagnosa(Long id,Diagnosa diagnosa);

    Map<String ,Boolean> deleteDiagnosaById(Long id);

    List<Diagnosa> allDiagnosa();
}
