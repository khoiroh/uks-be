package com.server.uks.service;

import com.server.uks.modal.Siswa;

import java.util.List;
import java.util.Map;

public interface SiswaService {

    Siswa getSiswa(Long id);

    Siswa addSiswa(Siswa siswa);

    Siswa editSiswa(Long id,Siswa siswa);

    Map<String ,Boolean> deleteSiswaById(Long id);

    List<Siswa> allGuru();
}
