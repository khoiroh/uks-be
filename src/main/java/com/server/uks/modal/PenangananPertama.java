package com.server.uks.modal;

import javax.persistence.*;

@Entity
@Table(name = "table_penanganan")
public class PenangananPertama {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_penanganan")
    private String namaPenanganan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenanganan() {
        return namaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        this.namaPenanganan = namaPenanganan;
    }
}
