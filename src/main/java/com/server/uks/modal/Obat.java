package com.server.uks.modal;

import javax.persistence.*;

@Entity
@Table(name = "table_obat")
public class Obat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_obat")
    private String namaObat;

    @Column(name = "stock")
    private int stock;

    @Column(name = "tanggal_expired")
    private String tanggalExpired;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(String tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }
}
