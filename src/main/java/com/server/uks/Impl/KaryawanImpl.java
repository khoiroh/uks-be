package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.Karyawan;
import com.server.uks.repository.KaryawanRepository;
import com.server.uks.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class KaryawanImpl implements KaryawanService {

    @Autowired
    KaryawanRepository karyawanRepository;

    @Override
    public Karyawan getKaryawan(Long id) {
        return karyawanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Karyawan addKaryawan(Karyawan karyawan) {
        karyawan.setNamaKaryawan(karyawan.getNamaKaryawan());
        karyawan.setAlamat(karyawan.getAlamat());
        karyawan.setTempatLahir(karyawan.getTempatLahir());
        karyawan.setTanggalLahir(karyawan.getTanggalLahir());
        return karyawanRepository.save(karyawan);
    }

    @Override
    public Karyawan editKaryawan(Long id, Karyawan karyawan) {
        Karyawan karyawan1 = karyawanRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        karyawan1.setNamaKaryawan(karyawan.getNamaKaryawan());
        karyawan1.setAlamat(karyawan.getAlamat());
        karyawan1.setTempatLahir(karyawan.getTempatLahir());
        karyawan1.setTanggalLahir(karyawan.getTanggalLahir());
        return karyawanRepository.save(karyawan1);
    }

    @Override
    public Map<String, Boolean> deleteKaryawanById(Long id) {
        try {
            karyawanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Karyawan> allKaryawan() {
        return karyawanRepository.findAll();
    }
}
