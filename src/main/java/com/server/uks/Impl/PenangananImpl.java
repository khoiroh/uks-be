package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.PenangananPertama;
import com.server.uks.repository.PenangananRepository;
import com.server.uks.service.PenangananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PenangananImpl implements PenangananService {

    @Autowired
    PenangananRepository penangananRepository;

    @Override
    public PenangananPertama getPenangananPertama(Long id) {
        return penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public PenangananPertama addPenangananPertama(PenangananPertama penangananPertama) {
        penangananPertama.setNamaPenanganan(penangananPertama.getNamaPenanganan());
        return penangananRepository.save(penangananPertama);
    }

    @Override
    public PenangananPertama editPenangananPertama(Long id, PenangananPertama penangananPertama) {
        PenangananPertama penangananPertama1 = penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        penangananPertama1.setNamaPenanganan(penangananPertama.getNamaPenanganan());
        return penangananRepository.save(penangananPertama1);
    }

    @Override
    public Map<String, Boolean> deletePenangananPertamaById(Long id) {
        try {
            penangananRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<PenangananPertama> allPenangananPertama() {
        return penangananRepository.findAll();
    }
}
