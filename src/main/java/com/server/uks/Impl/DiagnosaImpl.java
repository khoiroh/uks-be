package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.Diagnosa;
import com.server.uks.repository.DiagnosaRepository;
import com.server.uks.service.DiagnosaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DiagnosaImpl implements DiagnosaService {

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Override
    public Diagnosa getDiagnosa(Long id) {
        return diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Diagnosa addDiagnosa(Diagnosa diagnosa) {
        diagnosa.setNamaPenyakit(diagnosa.getNamaPenyakit());
        return diagnosaRepository.save(diagnosa);
    }

    @Override
    public Diagnosa editDiagnosa(Long id, Diagnosa diagnosa) {
        Diagnosa diagnosa1 = diagnosaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        diagnosa1.setNamaPenyakit(diagnosa.getNamaPenyakit());
        return diagnosaRepository.save(diagnosa1);
    }

    @Override
    public Map<String, Boolean> deleteDiagnosaById(Long id) {
        try {
            diagnosaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Diagnosa> allDiagnosa() {
        return diagnosaRepository.findAll();
    }
}
