package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.Tindakan;
import com.server.uks.repository.TindakanRepository;
import com.server.uks.service.TindakanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TindakanImpl implements TindakanService {

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
    public Tindakan getTindakan(Long id) {
        return tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Tindakan addTindakan(Tindakan tindakan) {
        tindakan.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tindakan);
    }

    @Override
    public Tindakan editTindakan(Long id, Tindakan tindakan) {
        Tindakan tindakan1 = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        tindakan1.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tindakan1);
    }

    @Override
    public Map<String, Boolean> deleteTindakanById(Long id) {
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Tindakan> allTindakan() {
        return tindakanRepository.findAll();
    }
}
