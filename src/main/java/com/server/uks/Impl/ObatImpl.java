package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.Obat;
import com.server.uks.repository.ObatRepository;
import com.server.uks.service.ObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ObatImpl implements ObatService {

    @Autowired
    ObatRepository obatRepository;

    @Override
    public Obat getObat(Long id) {
        return obatRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Obat addObat(Obat obat) {
        obat.setNamaObat(obat.getNamaObat());
        obat.setStock(obat.getStock());
        obat.setTanggalExpired(obat.getTanggalExpired());
        return obatRepository.save(obat);
    }

    @Override
    public Obat editObat(Long id, Obat obat) {
        Obat obat1 = obatRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        obat1.setNamaObat(obat.getNamaObat());
        obat1.setStock(obat.getStock());
        obat1.setTanggalExpired(obat.getTanggalExpired());
        return obatRepository.save(obat1);
    }

    @Override
    public Map<String, Boolean> deleteObatById(Long id) {
        try {
            obatRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Obat> allObat() {
        return obatRepository.findAll();
    }
}
