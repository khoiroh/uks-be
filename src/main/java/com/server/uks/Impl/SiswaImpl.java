package com.server.uks.Impl;

import com.server.uks.exception.NotFoundException;
import com.server.uks.modal.Siswa;
import com.server.uks.repository.SiswaRepository;
import com.server.uks.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SiswaImpl implements SiswaService {
    @Autowired
    SiswaRepository siswaRepository;


    @Override
    public Siswa getSiswa(Long id) {
        return siswaRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Siswa addSiswa(Siswa siswa) {
        siswa.setNamaSiswa(siswa.getNamaSiswa());
        siswa.setAlamat(siswa.getAlamat());
        siswa.setTempatLahir(siswa.getTempatLahir());
        siswa.setTanggalLahir(siswa.getTanggalLahir());
        siswa.setKelas(siswa.getKelas());
        return siswaRepository.save(siswa);
    }

    @Override
    public Siswa editSiswa(Long id, Siswa siswa) {
        Siswa siswa1 = siswaRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        siswa1.setNamaSiswa(siswa.getNamaSiswa());
        siswa1.setAlamat(siswa.getAlamat());
        siswa1.setTempatLahir(siswa.getTempatLahir());
        siswa1.setTanggalLahir(siswa.getTanggalLahir());
        siswa1.setKelas(siswa.getKelas());
        return siswaRepository.save(siswa1);
    }

    @Override
    public Map<String, Boolean> deleteSiswaById(Long id) {
        try {
            siswaRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Siswa> allGuru() {
        return siswaRepository.findAll();
    }
}
