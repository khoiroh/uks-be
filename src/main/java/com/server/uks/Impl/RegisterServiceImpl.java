package com.server.uks.Impl;

import com.server.uks.exception.InternalErrorException;
import com.server.uks.exception.NotFoundException;
import com.server.uks.jwt.JwtProvider;
import com.server.uks.modal.Login;
import com.server.uks.modal.Register;
import com.server.uks.repository.RegisterRepository;
import com.server.uks.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    RegisterRepository registerRepository;

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }



    @Override
    public Register registrasi(Register sekolah) {
        String UserPassword = sekolah.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(sekolah.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
        sekolah.setPassword(passwordEncoder.encode(sekolah.getPassword()));
        return registerRepository.save(sekolah);

    }

    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getEmail(), login.getPassword());
        Register sekolah ;

//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(login.getEmail()).matches();
        System.out.println("is Email " + isEmail);

//        jika true, akan menjalankan sistem if
        if(isEmail) {
            sekolah = registerRepository.findByEmail(login.getEmail());
        } else { // jika false, else akan dijalankan, dgn login username
            sekolah = registerRepository.findByUsername(login.getEmail());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", sekolah);
        return response;

    }

    @Override
    public Register getById(Long id) {
        return registerRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Register update(Long id, Register sekolah) {
        return null;
    }

    @Override
    public Map<String, Boolean> deleteSekolah(Long id) {
        try {
            registerRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }

    }

    @Override
    public Page<Register> getAll(String query, Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return registerRepository.findAll(query ,pageable);

    }
}
