package com.server.uks.repository;

import com.server.uks.modal.PenangananPertama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PenangananRepository extends JpaRepository<PenangananPertama, Long> {
}
