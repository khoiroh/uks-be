package com.server.uks.repository;

import com.server.uks.modal.Tindakan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TindakanRepository extends JpaRepository<Tindakan, Long> {
}
