package com.server.uks.repository;

import com.server.uks.modal.Register;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterRepository extends JpaRepository<Register, Long> {

    Register findByEmail(String email);

    @Query(value = "SELECT * FROM register  WHERE " +
            "email LIKE CONCAT('%',:query, '%')", nativeQuery = true)
    Page<Register> findAll(String query, Pageable pageable);

    @Query(value = "SELECT * FROM register  WHERE " +
            "username LIKE CONCAT('%',:username, '%')", nativeQuery = true)
    Register findByUsername(String username);

}
