package com.server.uks.repository;

import com.server.uks.modal.Guru;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GuruRepository extends JpaRepository<Guru, Long> {
    @Query(value = "SELECT * FROM table_guru  WHERE " +
            "nama_guru LIKE CONCAT('%',:query, '%')", nativeQuery = true)
    Page<Guru> findGuru(Pageable pageable);
}
