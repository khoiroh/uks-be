package com.server.uks.repository;

import com.server.uks.modal.Diagnosa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {
}
